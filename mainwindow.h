#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include "simulation.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void set_material(int i);
public slots:
    void material_placed(QPointF pt1, QPointF pt2);

    void simulation_tick();
private slots:
    void on_actionSelectU_triggered();

    void on_actionSelectN_triggered();

    void on_actionSelectH2O_triggered();

    void on_actionSelectErase_triggered();

    void on_actionSelectC_triggered();

    void on_actionSelectInspect_triggered();

private:
    Ui::MainWindow *ui;

    QGraphicsScene scene;
    QTimer *timer;
    class Simulation simulation;
    int inspectX, inspectY;
};

#endif // MAINWINDOW_H
