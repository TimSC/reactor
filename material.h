#ifndef MATERIAL_H
#define MATERIAL_H

#include <QGraphicsScene>
#include <QAbstractGraphicsShapeItem>
#include <memory>

class Material
{
public:
    Material();
    Material(const Material &obj);
    virtual ~Material();
    virtual Material& operator = (const Material &obj);

    virtual void AddToScene(QGraphicsScene *scene, int x, int y) = 0;
    virtual std::shared_ptr<Material> Clone() = 0;
    virtual bool SameType(std::shared_ptr<Material> mat) = 0;
    virtual bool SameType(const Material &mat) = 0;
    virtual void UpdateAppearance() {};
    virtual void TimeStep(double elapse);
    virtual void UpdateBoundary() {};
    virtual std::string Inspect();

    float freeFastNeutrons, freeSlowNeutrons;
    float temperature; //Kelvin
    bool inScene;

    float neutronHalfLife;
protected:
    QList<QAbstractGraphicsShapeItem *> drawables;
};

class Fuel : public Material
{
public:
    Fuel();
    Fuel(const Fuel &obj);
    virtual ~Fuel();
    Fuel& operator = (const Fuel &obj);
    virtual Fuel& operator = (const Material &obj);

    virtual void AddToScene(QGraphicsScene *scene, int x, int y);
    virtual std::shared_ptr<Material> Clone();
    virtual bool SameType(std::shared_ptr<Material> mat);
    virtual bool SameType(const Material &mat);
    virtual void TimeStep(double elapse);
    virtual std::string Inspect();

    float enrichment = 0.03;
};

class NeutronSource : public Material
{
public:
    NeutronSource();
    NeutronSource(const NeutronSource &obj);
    virtual ~NeutronSource();
    NeutronSource& operator = (const NeutronSource &obj);
    virtual NeutronSource& operator = (const Material &obj);

    virtual void AddToScene(QGraphicsScene *scene, int x, int y);
    virtual std::shared_ptr<Material> Clone();
    virtual bool SameType(std::shared_ptr<Material> mat);
    virtual bool SameType(const Material &mat);
    virtual void TimeStep(double elapse);
    virtual std::string Inspect();

    float strength;
};

class Air : public Material
{
public:
    Air();
    Air(const Air &obj);
    virtual ~Air();
    Air& operator = (const Air &obj);
    virtual Air& operator = (const Material &obj);

    virtual void AddToScene(QGraphicsScene *scene, int x, int y);
    virtual std::shared_ptr<Material> Clone();
    virtual bool SameType(std::shared_ptr<Material> mat);
    virtual bool SameType(const Material &mat);
    virtual void UpdateAppearance();
    virtual void UpdateBoundary();
    virtual std::string Inspect();
};

class Graphite : public Material
{
public:
    Graphite();
    Graphite(const Graphite &obj);
    virtual ~Graphite();
    Graphite& operator = (const Graphite &obj);
    virtual Graphite& operator = (const Material &obj);

    virtual void AddToScene(QGraphicsScene *scene, int x, int y);
    virtual std::shared_ptr<Material> Clone();
    virtual bool SameType(std::shared_ptr<Material> mat);
    virtual bool SameType(const Material &mat);
    virtual void UpdateAppearance();
    virtual void TimeStep(double elapse);
    virtual std::string Inspect();
};

class Coolant : public Material
{
public:
    Coolant();
    Coolant(const Coolant &obj);
    virtual ~Coolant();
    Coolant& operator = (const Coolant &obj);
    virtual Coolant& operator = (const Material &obj);

    virtual void AddToScene(QGraphicsScene *scene, int x, int y);
    virtual std::shared_ptr<Material> Clone();
    virtual bool SameType(std::shared_ptr<Material> mat);
    virtual bool SameType(const Material &mat);
    virtual void UpdateAppearance();
    virtual void TimeStep(double elapse);
    virtual std::string Inspect();
};

#endif // MATERIAL_H
