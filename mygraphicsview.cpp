#include "mygraphicsview.h"
#include <iostream>
#include <QScrollBar>
#include <cmath>
using namespace std;

MyGraphicsView::MyGraphicsView(QWidget *parent) : QGraphicsView(parent)
{
    viewScale = 0;
    _pan = false;
    _selectdrag = false;
    _panStartX = 0; _panStartY = 0; _dragStartX = 0; _dragStartY = 0;
    _dragEndX = 0; _dragEndY = 0;
}

void MyGraphicsView::wheelEvent(QWheelEvent *event)
{
    QPoint ang = event->angleDelta();
    //cout << "wheelevent " << (ang.ry()/8.0) << endl;

    if(ang.ry()>0) viewScale ++;
    if(ang.ry()<0) viewScale --;
    if(viewScale < -3) viewScale = -3;
    if(viewScale > 3) viewScale = 3;

    float viewScaleF = pow(2.0, viewScale);
    this->resetTransform();
    this->scale(viewScaleF, viewScaleF);

    event->accept();
}

void MyGraphicsView::drawBackground(QPainter *painter, const QRectF &rect)
{
    //cout << rect.x() << "," << rect.y() << "," << rect.width() << "," << rect.height() << endl;

    //Based on https://www.qtcentre.org/threads/27364-Trying-to-draw-a-grid-on-a-QGraphicsScene-View?p=129770#post129770
    int gridInterval = 100; //interval to draw grid lines at
    painter->setWorldMatrixEnabled(true);

    qreal left = int(rect.left()) - (int(rect.left()) % gridInterval );
    qreal top = int(rect.top()) - (int(rect.top()) % gridInterval );

    QVarLengthArray<QLineF, 100> linesX;
    for (qreal x = left; x < rect.right(); x += gridInterval )
        linesX.append(QLineF(x, rect.top(), x, rect.bottom()));

    QVarLengthArray<QLineF, 100> linesY;
    for (qreal y = top; y < rect.bottom(); y += gridInterval )
            linesY.append(QLineF(rect.left(), y, rect.right(), y));

    painter->drawLines(linesX.data(), linesX.size());
    painter->drawLines(linesY.data(), linesY.size());
}

void MyGraphicsView::drawForeground(QPainter *painter, const QRectF &rect)
{
    if (_selectdrag)
    {
        painter->drawLine(drag_s, QPointF(drag_s.x(), drag_e.y()));
        painter->drawLine(QPointF(drag_s.x(), drag_e.y()), drag_e);
        painter->drawLine(drag_e, QPointF(drag_e.x(), drag_s.y()));
        painter->drawLine(QPointF(drag_e.x(), drag_s.y()), drag_s);
    }
}

void MyGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        _selectdrag = true;
        _dragStartX = event->x();
        _dragStartY = event->y();
        event->accept();
        return;
    }
    else if (event->button() == Qt::RightButton)
    {
        //Drag scrolling https://stackoverflow.com/a/5156978/4288232
        _pan = true;
        _panStartX = event->x();
        _panStartY = event->y();
        setCursor(Qt::ClosedHandCursor);
        event->accept();
        return;
    }
    else
        QGraphicsView::mousePressEvent(event);
}

void MyGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        _dragEndX = event->x();
        _dragEndY = event->y();
        drag_s = this->mapToScene(_dragStartX, _dragStartY);
        drag_e = this->mapToScene(_dragEndX, _dragEndY);

        QPointF tl(min(drag_s.x(), drag_e.x()), min(drag_s.y(), drag_e.y()));
        QPointF br(max(drag_s.x(), drag_e.x()), max(drag_s.y(), drag_e.y()));

        this->areaSelected(tl, br);
        scene()->update();
        _selectdrag = false;
        event->accept();

        return;
    }
    else if (event->button() == Qt::RightButton)
    {
        _pan = false;
        setCursor(Qt::ArrowCursor);
        event->accept();

        return;
    }
    else
        QGraphicsView::mouseReleaseEvent(event);
}

void MyGraphicsView::mouseMoveEvent(QMouseEvent *event)
{
    if (_selectdrag)
    {
        _dragEndX = event->x();
        _dragEndY = event->y();
        drag_s = this->mapToScene(_dragStartX, _dragStartY);
        drag_e = this->mapToScene(_dragEndX, _dragEndY);

        scene()->update();
        event->accept();
        return;
    }
    else if (_pan)
    {
        horizontalScrollBar()->setValue(horizontalScrollBar()->value() - (event->x() - _panStartX));
        verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - _panStartY));
        _panStartX = event->x();
        _panStartY = event->y();
        event->accept();
        return;
    }
    else
        QGraphicsView::mouseMoveEvent(event);
}

void MyGraphicsView::enterEvent(QEvent *event)
{
    QGraphicsView::enterEvent(event);
}
