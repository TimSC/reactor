#include "material.h"
#include <iostream>
#include <cmath>
#include <sstream>
using namespace std;

const float LN2 = log(2.0f);

Material::Material()
{
    freeFastNeutrons = 0.0f;
    freeSlowNeutrons = 0.0f;
    temperature = 293;
    inScene = false;
    neutronHalfLife = 10.0;
}

Material::Material(const Material &obj)
{
    *this = obj;
}

Material::~Material()
{
    for(int i=0; i<this->drawables.size(); i++)
    {
        this->drawables[i]->scene()->removeItem(this->drawables[i]);
    }
    this->drawables.clear();
}

Material& Material::operator = (const Material &obj)
{
    freeFastNeutrons = obj.freeFastNeutrons;
    freeSlowNeutrons = obj.freeSlowNeutrons;
    return *this;
}

void Material::TimeStep(double elapse)
{
    //Neutron decay (free neutrons are unstable)
    float lambda = LN2 / neutronHalfLife;
    freeFastNeutrons *= exp(-lambda * elapse);
    freeSlowNeutrons *= exp(-lambda * elapse);
}

std::string Material::Inspect()
{
    stringstream ss;
    ss << "Fast neutrons " << freeFastNeutrons << endl;
    ss << "Slow neutrons " << freeSlowNeutrons << endl;
    ss << "Temperature " << temperature << "K" << endl;
    return ss.str();
}

// ********************************************************************

Fuel::Fuel() : Material()
{

}

Fuel::Fuel(const Fuel &obj) : Material()
{
    *this = obj;
}

Fuel::~Fuel()
{

}

Fuel& Fuel::operator = (const Fuel &obj)
{
    Material::operator =(obj);
    return *this;
}

Fuel& Fuel::operator = (const Material &obj)
{
    if (SameType(obj))
        *this = *dynamic_cast<const class Fuel *>(&obj);
    else
        Material::operator =(obj);
    return *this;
}

void Fuel::AddToScene(QGraphicsScene *scene, int x, int y)
{
    QGraphicsRectItem *item = scene->addRect(x*100+1, y*100+1,
                100-2, 100-2,
                QPen(QColor(128,128,0)),QBrush(QColor(255,255,0)) );
    this->drawables.append(item);
    inScene = true;
}

std::shared_ptr<Material> Fuel::Clone()
{
    return make_shared<class Fuel>(*this);
}

bool Fuel::SameType(std::shared_ptr<Material> mat)
{
    class Fuel *f = dynamic_cast<class Fuel *>(mat.get());
    return f != nullptr;
}

bool Fuel::SameType(const Material &mat)
{
    const class Fuel *f = dynamic_cast<const class Fuel *>(&mat);
    return f != nullptr;
}

void Fuel::TimeStep(double elapse)
{
    //TODO Make fuel reactivity vary by temperature

    //Calc neutrons absorbed
    float nSlowAbsorb = 1.0f * this->freeSlowNeutrons;
    this->freeSlowNeutrons -= nSlowAbsorb;
    temperature += nSlowAbsorb * 0.1f;

    float nFastAbsorb = (1.0f - enrichment) * 0.001f * this->freeFastNeutrons
            + enrichment * 1.0f * this->freeFastNeutrons;
    this->freeFastNeutrons -= nFastAbsorb;
    temperature += nFastAbsorb * 1.0f;

    //Do nuclear fisson
    float nAbsorb = (nSlowAbsorb + nFastAbsorb);
    temperature += nAbsorb * 3.0f; //Create heat
    this->freeFastNeutrons += 3.0f * nAbsorb; //Releases more neutrons

    //TODO breeder effect
    //TODO delayed neutron release/slower thermal release
    //TODO poisoning by xenon etc

}

std::string Fuel::Inspect()
{
    stringstream ss;
    ss << "Fuel" << endl;
    ss << Material::Inspect();
    ss << "Enrichment " << (enrichment*100.0f) << "%" << endl;
    return ss.str();
}

// ********************************************************************

NeutronSource::NeutronSource() : Material()
{
    strength = 0.8;
}

NeutronSource::NeutronSource(const NeutronSource &obj) : Material()
{
    *this = obj;
}

NeutronSource::~NeutronSource()
{

}

NeutronSource& NeutronSource::operator = (const NeutronSource &obj)
{
    Material::operator =(obj);
    strength = obj.strength;
    return *this;
}

NeutronSource& NeutronSource::operator = (const Material &obj)
{
    if (SameType(obj))
        *this = *dynamic_cast<const class NeutronSource *>(&obj);
    else
        Material::operator =(obj);
    return *this;
}

void NeutronSource::AddToScene(QGraphicsScene *scene, int x, int y)
{
    QGraphicsRectItem *item = scene->addRect(x*100+1, y*100+1,
                100-2, 100-2,
                QPen(QColor(0,128,0)),QBrush(QColor(0,255,0)) );
    this->drawables.append(item);
    inScene = true;
}

std::shared_ptr<Material> NeutronSource::Clone()
{
    return make_shared<class NeutronSource>(*this);
}

bool NeutronSource::SameType(std::shared_ptr<Material> mat)
{
    class NeutronSource *f = dynamic_cast<class NeutronSource *>(mat.get());
    return f != nullptr;
}

bool NeutronSource::SameType(const Material &mat)
{
    const class NeutronSource *f = dynamic_cast<const class NeutronSource *>(&mat);
    return f != nullptr;
}

void NeutronSource::TimeStep(double elapse)
{
    Material::TimeStep(elapse);

    //Neutron production
    freeFastNeutrons += elapse * strength;
}

std::string NeutronSource::Inspect()
{
    stringstream ss;
    ss << "Neutron source" << endl;
    ss << Material::Inspect();
    ss << "Source strength " << this->strength << endl;
    return ss.str();
}

// ********************************************************************

Air::Air() : Material()
{

}

Air::Air(const Air &obj) : Material()
{
    *this = obj;
}

Air::~Air()
{

}

Air& Air::operator = (const Air &obj)
{
    Material::operator =(obj);
    return *this;
}

Air& Air::operator = (const Material &obj)
{
    if (SameType(obj))
        *this = *dynamic_cast<const class Air *>(&obj);
    else
        Material::operator =(obj);
    return *this;
}

void Air::AddToScene(QGraphicsScene *scene, int x, int y)
{
    QGraphicsRectItem *item = scene->addRect(x*100+1, y*100+1,
                100-2, 100-2,
                QPen(QColor(255,255,255)),QBrush(QColor(255,255,255)) );
    this->drawables.append(item);
    inScene = true;
}

std::shared_ptr<Material> Air::Clone()
{
    return make_shared<class Air>(*this);
}

bool Air::SameType(std::shared_ptr<Material> mat)
{
    class Air *f = dynamic_cast<class Air *>(mat.get());
    return f != nullptr;
}

bool Air::SameType(const Material &mat)
{
    const class Air *f = dynamic_cast<const class Air *>(&mat);
    return f != nullptr;
}

void Air::UpdateAppearance()
{
    if(this->drawables.size() < 1) return;
    QGraphicsRectItem *rect = dynamic_cast<QGraphicsRectItem *> (this->drawables[0]);
    if(rect==nullptr) return;

    rect->setBrush(QBrush(QColor(255.0f*max(1.0-this->freeFastNeutrons, 0.0),
                                 255.0f*max(1.0-this->freeSlowNeutrons, 0.0),
                                 255.0f*max(1.0-this->freeFastNeutrons, 0.0)*max(1.0-this->freeSlowNeutrons, 0.0))));
}

void Air::UpdateBoundary()
{
    freeFastNeutrons = 0.0f;
    freeSlowNeutrons = 0.0f;
}

std::string Air::Inspect()
{
    stringstream ss;
    ss << "Air" << endl;
    ss << Material::Inspect();
    return ss.str();
}

// ********************************************************************

Graphite::Graphite() : Material()
{

}

Graphite::Graphite(const Graphite &obj) : Material()
{
    *this = obj;
}

Graphite::~Graphite()
{

}

Graphite& Graphite::operator = (const Graphite &obj)
{
    Material::operator =(obj);
    return *this;
}

Graphite& Graphite::operator = (const Material &obj)
{
    if (SameType(obj))
        *this = *dynamic_cast<const class Graphite *>(&obj);
    else
        Material::operator =(obj);
    return *this;
}

void Graphite::AddToScene(QGraphicsScene *scene, int x, int y)
{
    QGraphicsRectItem *item = scene->addRect(x*100+1, y*100+1,
                100-2, 100-2,
                QPen(QColor(64,64,64)),QBrush(QColor(128,128,128)) );
    this->drawables.append(item);
    inScene = true;
}

std::shared_ptr<Material> Graphite::Clone()
{
    return make_shared<class Graphite>(*this);
}

bool Graphite::SameType(std::shared_ptr<Material> mat)
{
    class Graphite *f = dynamic_cast<class Graphite *>(mat.get());
    return f != nullptr;
}

bool Graphite::SameType(const Material &mat)
{
    const class Graphite *f = dynamic_cast<const class Graphite *>(&mat);
    return f != nullptr;
}

void Graphite::UpdateAppearance()
{

}

void Graphite::TimeStep(double elapse)
{
    Material::TimeStep(elapse);

    //Moderate fast neutrons to slow neutrons
    float convHalfLife = 2.0;
    float lambda = LN2 / convHalfLife;
    float nconv = exp(-lambda * elapse) * freeFastNeutrons;
    freeSlowNeutrons += nconv;
    temperature += nconv * 1.0f;
    freeFastNeutrons -= nconv;
}

std::string Graphite::Inspect()
{
    stringstream ss;
    ss << "Graphite" << endl;
    ss << Material::Inspect();
    return ss.str();
}


// ********************************************************************

Coolant::Coolant() : Material()
{

}

Coolant::Coolant(const Coolant &obj) : Material()
{
    *this = obj;
}

Coolant::~Coolant()
{

}

Coolant& Coolant::operator = (const Coolant &obj)
{
    Material::operator =(obj);
    return *this;
}

Coolant& Coolant::operator = (const Material &obj)
{
    if (SameType(obj))
        *this = *dynamic_cast<const class Coolant *>(&obj);
    else
        Material::operator =(obj);
    return *this;
}

void Coolant::AddToScene(QGraphicsScene *scene, int x, int y)
{
    QGraphicsRectItem *item = scene->addRect(x*100+1, y*100+1,
                100-2, 100-2,
                QPen(QColor(0,0,128)),QBrush(QColor(0,0,192)) );
    this->drawables.append(item);
    inScene = true;
}

std::shared_ptr<Material> Coolant::Clone()
{
    return make_shared<class Coolant>(*this);
}

bool Coolant::SameType(std::shared_ptr<Material> mat)
{
    class Coolant *f = dynamic_cast<class Coolant *>(mat.get());
    return f != nullptr;
}

bool Coolant::SameType(const Material &mat)
{
    const class Coolant *f = dynamic_cast<const class Coolant *>(&mat);
    return f != nullptr;
}

void Coolant::UpdateAppearance()
{

}

void Coolant::TimeStep(double elapse)
{
    Material::TimeStep(elapse);

    //Moderate fast neutrons to slow neutrons
    float convHalfLife = 200.0;
    float lambda = LN2 / convHalfLife;
    float nconv = exp(-lambda * elapse) * freeFastNeutrons;
    freeSlowNeutrons += nconv;
    temperature += nconv * 1.0f;
    freeFastNeutrons -= nconv;

    //Absorb neutrons
    float absorbHalfLife = 2.0;
    lambda = LN2 / absorbHalfLife;
    nconv = exp(-lambda * elapse) * freeFastNeutrons;
    temperature += nconv * 1.0f;
    freeFastNeutrons -= nconv;

    //Simplified cooling without flow
    float dt = 0.5*(temperature - 293.0f);
    temperature -= dt;

    //TODO void coefficient

}

std::string Coolant::Inspect()
{
    stringstream ss;
    ss << "Coolant" << endl;
    ss << Material::Inspect();
    return ss.str();
}
