#ifndef SIMULATION_H
#define SIMULATION_H

#include "material.h"
#include <map>
#include <memory>
#include <string>

typedef std::map<int, std::shared_ptr<class Material> > MaterialCol;
typedef std::map<int, MaterialCol > MaterialMap;

class Simulation
{
public:
    Simulation();

    void AddMaterial(int x, int y, std::shared_ptr<class Material> mat);
    void EraseMaterial(int x, int y);

    void TimeStep(double elapse);
    void UpdateAppearance(QGraphicsScene *scene);
    void ClipToSize(MaterialMap &matMap, int paddedXMin, int paddedXMax, int paddedYMin, int paddedYMax);
    std::string Inspect(int inspectX, int inspectY);
protected:
    MaterialMap materials, materialsTplusOne;
    void UpdateExtent();
    bool extentDefined;
    int extentXMin, extentXMax, extentYMin, extentYMax;
};

#endif // SIMULATION_H
