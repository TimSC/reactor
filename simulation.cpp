#include "simulation.h"
#include "material.h"
#include <iostream>
using namespace std;

Simulation::Simulation()
{
    extentDefined = false;
    extentXMin = 0; extentXMax = 0; extentYMin = 0; extentYMax = 0;
}

void Simulation::AddMaterial(int x, int y, std::shared_ptr<class Material> mat)
{
    auto it = materials.find(x);
    if(it == materials.end())
    {
        MaterialCol empty;
        materials[x] = empty;
        it = materials.find(x);
    }

    MaterialCol &col = it->second;
    col[y] = mat;

    if(!extentDefined)
    {
        extentXMin = x;
        extentXMax = x;
        extentYMin = y;
        extentYMax = y;
        extentDefined = true;
    }
    else
    {
        if(extentXMin > x) extentXMin = x;
        if(extentXMax < x) extentXMax = x;
        if(extentYMin > y) extentYMin = y;
        if(extentYMax < y) extentYMax = y;
    }

}

void Simulation::EraseMaterial(int x, int y)
{
    auto it = materials.find(x);
    if(it == materials.end()) return;

    MaterialCol &col = it->second;
    auto it2 = col.find(y);
    if(it2 != col.end())
        col.erase(it2);

    this->UpdateExtent();
}

void Simulation::UpdateExtent()
{
    class Air air;
    //Find extent of materials
    extentDefined = false;
    for(auto it=materials.begin(); it!=materials.end(); it++)
    {
        MaterialCol &col = it->second;
        for(auto it2=col.begin(); it2 != col.end(); it2++)
        {
            std::shared_ptr<class Material> mat = it2->second;
            if(mat->SameType(air)) continue;

            if(!extentDefined)
            {
                extentXMin = it->first;
                extentXMax = it->first;
                extentYMin = it2->first;
                extentYMax = it2->first;
                extentDefined = true;
            }
            else
            {
                if(extentXMin > it->first) extentXMin = it->first;
                if(extentXMax < it->first) extentXMax = it->first;
                if(extentYMin > it2->first) extentYMin = it2->first;
                if(extentYMax < it2->first) extentYMax = it2->first;
            }
        }

    }
}

void Simulation::ClipToSize(MaterialMap &matMap, int paddedXMin, int paddedXMax, int paddedYMin, int paddedYMax)
{
    //Remove unnessary padding
    for(auto srcIt = matMap.begin(); srcIt != matMap.end(); )
    {
        bool removePrev = false;

        if(srcIt->first < paddedXMin)
            removePrev = true;
        if(srcIt->first >= paddedXMax)
            removePrev = true;

        auto prevIt = srcIt;
        srcIt++;
        if(removePrev)
            matMap.erase(prevIt);
    }

    for(auto srcIt = matMap.begin(); srcIt != matMap.end(); srcIt++)
    {
        MaterialCol &col = srcIt->second;
        for(auto srcIt2 = col.begin(); srcIt2 != col.end(); )
        {
            bool removePrev = false;

            if(srcIt2->first < paddedYMin)
                removePrev = true;
            if(srcIt2->first >= paddedYMax)
                removePrev = true;

            auto prevIt = srcIt2;
            srcIt2++;
            if(removePrev)
                col.erase(prevIt);
        }
    }
}

void Simulation::TimeStep(double elapse)
{
    cout << "tick " << elapse << endl;

    if(!extentDefined)
    {
        this->materials.clear();
        this->materialsTplusOne.clear();
        return;
    }

    //Pad materials with a margin of air
    int pad = 4;
    int paddedXMin = extentXMin-pad;
    int paddedXMax = extentXMax+pad+1;
    int paddedYMin = extentYMin-pad;
    int paddedYMax = extentYMax+pad+1;
    for(int x=paddedXMin; x<paddedXMax; x++)
    {
        MaterialCol &col = materials[x];
        for(int y=paddedYMin; y<paddedYMax; y++)
        {
            auto it = col.find(y);
            if(it == col.end())
            {
                col[y] = std::make_shared<class Air>();
            }
        }
    }

    this->ClipToSize(materials, paddedXMin, paddedXMax, paddedYMin, paddedYMax);

    //Update internal material states
    for(auto srcIt = materials.begin(); srcIt != materials.end(); srcIt++)
    {
        MaterialCol &srcCol = srcIt->second;

        for(auto srcIt2 = srcCol.begin(); srcIt2 != srcCol.end(); srcIt2++)
        {
            shared_ptr<Material> &mat = srcIt2->second;
            mat->TimeStep(elapse);
        }
    }

    //Handle simulation boundaries
    for(int x=paddedXMin; x<paddedXMax; x++)
    {
        MaterialCol &col = materials[x];
        col[paddedYMin]->UpdateBoundary();
        col[paddedYMax-1]->UpdateBoundary();
    }
    MaterialCol &leftEdge = materials[paddedXMin];
    MaterialCol &rightEdge = materials[paddedXMax-1];
    for(int y=paddedYMin+1; y<paddedYMax-1; y++)
    {
        leftEdge[y]->UpdateBoundary();
        rightEdge[y]->UpdateBoundary();
    }

    this->ClipToSize(materialsTplusOne, paddedXMin, paddedXMax, paddedYMin, paddedYMax);

    //Duplicate materials to materialsTplusOne
    int newMats = 0, updatesMats = 0;
    for(auto srcIt = materials.begin(); srcIt != materials.end(); srcIt++)
    {
        MaterialCol &srcCol = srcIt->second;
        MaterialCol &dstCol = materialsTplusOne[srcIt->first];

        for(auto srcIt2 = srcCol.begin(); srcIt2 != srcCol.end(); srcIt2++)
        {
            auto dstIt2 = dstCol.find(srcIt2->first);
            if(dstIt2 == dstCol.end() || !dstIt2->second->SameType(srcIt2->second))
            {
                dstCol[srcIt2->first] = srcIt2->second->Clone();
                newMats ++;
            }
            else
            {
                *dstIt2->second = *srcIt2->second;
                updatesMats ++;
            }
        }
    }
    cout << "newMats " << newMats << endl;
    cout << "updatesMats " << updatesMats << endl;

    //Calculate neutron diffision (horizontal)
    float diffusionFastNeutron = 0.2, diffusionSlowNeutron = 0.2;
    float diffusionThermal = 0.1;
    for(int x=paddedXMin; x<paddedXMax-1; x++)
    {
        MaterialCol &src_col = materials[x];
        MaterialCol &next_src_col = materials[x+1];
        MaterialCol &dst_col = materialsTplusOne[x];
        MaterialCol &next_dst_col = materialsTplusOne[x+1];
        for(int y=paddedYMin; y<paddedYMax; y++)
        {
            shared_ptr<Material> &src_mat = src_col[y];
            shared_ptr<Material> &next_src_mat = next_src_col[y];
            shared_ptr<Material> &dst_mat = dst_col[y];
            shared_ptr<Material> &next_dst_mat = next_dst_col[y];

            float dfn = next_src_mat->freeFastNeutrons - src_mat->freeFastNeutrons;
            next_dst_mat->freeFastNeutrons -= dfn * diffusionFastNeutron;
            dst_mat->freeFastNeutrons += dfn * diffusionFastNeutron;

            float dsn = next_src_mat->freeSlowNeutrons - src_mat->freeSlowNeutrons;
            next_dst_mat->freeSlowNeutrons -= dsn * diffusionSlowNeutron;
            dst_mat->freeSlowNeutrons += dsn * diffusionSlowNeutron;

            float dt = next_src_mat->temperature - src_mat->temperature;
            next_dst_mat->temperature -= dsn * diffusionThermal;
            dst_mat->temperature += dsn * diffusionThermal;
        }
    }

    //Calculate neutron diffision (vertical)
    for(int x=paddedXMin; x<paddedXMax; x++)
    {
        MaterialCol &src_col = materials[x];
        MaterialCol &dst_col = materialsTplusOne[x];
        for(int y=paddedYMin; y<paddedYMax-1; y++)
        {
            shared_ptr<Material> &src_mat = src_col[y];
            shared_ptr<Material> &next_src_mat = src_col[y+1];
            shared_ptr<Material> &dst_mat = dst_col[y];
            shared_ptr<Material> &next_dst_mat = dst_col[y+1];

            float dfn = next_src_mat->freeFastNeutrons - src_mat->freeFastNeutrons;
            next_dst_mat->freeFastNeutrons -= dfn * diffusionFastNeutron;
            dst_mat->freeFastNeutrons += dfn * diffusionFastNeutron;

            float dsn = next_src_mat->freeSlowNeutrons - src_mat->freeSlowNeutrons;
            next_dst_mat->freeSlowNeutrons -= dsn * diffusionSlowNeutron;
            dst_mat->freeSlowNeutrons += dsn * diffusionSlowNeutron;

            float dt = next_src_mat->temperature - src_mat->temperature;
            next_dst_mat->temperature -= dsn * diffusionThermal;
            dst_mat->temperature += dsn * diffusionThermal;
        }

    }


    //Copy values from materialsTplusOne to materials
    for(auto srcIt = materialsTplusOne.begin(); srcIt != materialsTplusOne.end(); srcIt++)
    {
        MaterialCol &srcCol = srcIt->second;
        MaterialCol &dstCol = materials[srcIt->first];

        for(auto srcIt2 = srcCol.begin(); srcIt2 != srcCol.end(); srcIt2++)
        {
            shared_ptr<Material> &dst_mat = dstCol[srcIt2->first];
            *dst_mat = *srcIt2->second;
        }
    }
}

void Simulation::UpdateAppearance(QGraphicsScene *scene)
{
    for(auto srcIt = materials.begin(); srcIt != materials.end(); srcIt++)
    {
        MaterialCol &srcCol = srcIt->second;

        for(auto srcIt2 = srcCol.begin(); srcIt2 != srcCol.end(); srcIt2++)
        {
            shared_ptr<Material> &mat = srcIt2->second;
            if (!mat->inScene)
                mat->AddToScene(scene, srcIt->first, srcIt2->first);

            mat->UpdateAppearance();
        }
    }
}

std::string Simulation::Inspect(int inspectX, int inspectY)
{
    auto it = materials.find(inspectX);
    if(it == materials.end()) return "No material";
    MaterialCol &col = it->second;

    auto it2 = col.find(inspectY);
    if(it2 == col.end()) return "No material";

    shared_ptr<Material> mat = it2->second;
    return mat->Inspect();
}
