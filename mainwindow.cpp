#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <cmath>
using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    inspectX = 0;
    inspectY = 0;

    ui->setupUi(this);

    ui->actionSelectInspect->setChecked(true);
    ui->graphicsView->setScene(&this->scene);

    this->scene.setSceneRect((float)0,(float)0,(float)INT_MAX,(float)INT_MAX);

    connect(ui->graphicsView, &MyGraphicsView::areaSelected, this, &MainWindow::material_placed);

    timer = new QTimer(this);

    connect(timer, &QTimer::timeout, this, &MainWindow::simulation_tick);
    timer->start(1000);
}

MainWindow::~MainWindow()
{
    delete timer;
    delete ui;
}

void MainWindow::set_material(int i)
{
    this->ui->actionSelectInspect->setChecked(i==0);
    this->ui->actionSelectErase->setChecked(i==1);
    this->ui->actionSelectU->setChecked(i==2);
    this->ui->actionSelectN->setChecked(i==3);
    this->ui->actionSelectH2O->setChecked(i==4);
    this->ui->actionSelectC->setChecked(i==5);
}

void MainWindow::on_actionSelectInspect_triggered()
{
    set_material(0);
}

void MainWindow::on_actionSelectErase_triggered()
{
    set_material(1);
}

void MainWindow::on_actionSelectU_triggered()
{
    set_material(2);
}

void MainWindow::on_actionSelectN_triggered()
{
    set_material(3);
}

void MainWindow::on_actionSelectH2O_triggered()
{
    set_material(4);
}

void MainWindow::on_actionSelectC_triggered()
{
    set_material(5);
}

void MainWindow::material_placed(QPointF pt1, QPointF pt2)
{
    QPoint pt1g(floor(pt1.x()/100.0), floor(pt1.y()/100.0));
    QPoint pt2g(ceil(pt2.x()/100.0), ceil(pt2.y()/100.0));

    std::shared_ptr<class Material> mat;
    bool eraseMode = false;
    bool inspectMode = false;

    if(this->ui->actionSelectInspect->isChecked())
        inspectMode = true;
    else if(this->ui->actionSelectU->isChecked())
        mat = std::make_shared<class Fuel>();
    else if(this->ui->actionSelectN->isChecked())
        mat = std::make_shared<class NeutronSource>();
    else if(this->ui->actionSelectErase->isChecked())
        eraseMode = true;
    else if(this->ui->actionSelectC->isChecked())
        mat = std::make_shared<class Graphite>();
    else if(this->ui->actionSelectH2O->isChecked())
        mat = std::make_shared<class Coolant>();

    if(mat)
    {
        for(int x=pt1g.x(); x < pt2g.x(); x++)
            for(int y=pt1g.y(); y < pt2g.y(); y++)
            {
                std::shared_ptr<class Material> matToAdd = mat->Clone();
                matToAdd->AddToScene(&this->scene, x, y);
                simulation.AddMaterial(x, y, matToAdd);
            }
    }

    else if(eraseMode)
    {
        for(int x=pt1g.x(); x < pt2g.x(); x++)
            for(int y=pt1g.y(); y < pt2g.y(); y++)
            {
                simulation.EraseMaterial(x, y);
            }
    }

    else if(inspectMode)
    {
        inspectX = pt1g.x();
        inspectY = pt1g.y();
        ui->inspectTextEdit->setPlainText(QString(this->simulation.Inspect(inspectX, inspectY).c_str()));
    }
}

void MainWindow::simulation_tick()
{
    this->simulation.TimeStep(1.0);

    this->simulation.UpdateAppearance(&scene);

    ui->inspectTextEdit->setPlainText(QString(this->simulation.Inspect(inspectX, inspectY).c_str()));

}

