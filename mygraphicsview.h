#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QWheelEvent>

class MyGraphicsView : public QGraphicsView
{
    Q_OBJECT

public:
    MyGraphicsView(QWidget *parent=nullptr);

protected:
    void wheelEvent(QWheelEvent *event);
    void drawBackground(QPainter *painter, const QRectF &rect);
    void drawForeground(QPainter *painter, const QRectF &rect);

    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);

    int viewScale;
    bool _pan;
    int _panStartX, _panStartY, _dragStartX, _dragStartY, _dragEndX, _dragEndY;
    QPointF drag_s, drag_e;
    bool _selectdrag;

signals:
    void areaSelected(QPointF pt1, QPointF pt2);
};

#endif // MYGRAPHICSVIEW_H
